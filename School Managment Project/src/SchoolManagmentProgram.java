import java.util.List;

import enums.Gender;
import managers.*;
import models.*;

public class SchoolManagmentProgram {
	private SchoolManager schoolManager;
	private StudentManager studentManager;

	public SchoolManagmentProgram() {
		schoolManager = new SchoolManager();
		studentManager = new StudentManager();

		schoolManager.createHighSchools(3);
		schoolManager.createUniversities(2);

		createStudentsBySchool(schoolManager.getHighSchools());
		createStudentsBySchool(schoolManager.getUniversities());
	}

	public void printAverageGrade() {
		System.out.println("\n Agerage grades by schools: ");
		printAverageGradeBySchool(schoolManager.getHighSchools());
		printAverageGradeBySchool(schoolManager.getUniversities());
	}

	public void printTopPerformingStudent() {
		System.out.println("\n Top performing student: \n-> " + studentManager.findTopPerformingStudent().getName());
	}

	public void printTopPerformersByGender() {
		System.out.println("\n Top performing students by gender");
		System.out.println("-> Top FEMALE " + studentManager.findTopPerformingStudentByGender(Gender.FEMALE).getName());
		System.out.println("-> Top MALE " + studentManager.findTopPerformingStudentByGender(Gender.MALE).getName());
	}

	public void printTotalSchoolsIncome() {
		System.out.println("\n Total incomes");
		printIncomeBySchool(schoolManager.getHighSchools());
		printIncomeBySchool(schoolManager.getUniversities());
	}

	public void printTopContributer() {
		System.out.println("\n Top contributer:\n-> " + studentManager.findTopContributer().getName());
	}

	private void createStudentsBySchool(List<School> schools) {
		for (School school : schools) {
			List<Student> newHighSchoolStudents = studentManager.createStudents(10, school);
			schoolManager.sendMessage(newHighSchoolStudents, school);
		}
	}

	private void printAverageGradeBySchool(List<School> schools) {
		for (School school : schools) {
			List<Student> foundedStudents = studentManager.findAllStudentsBySchoolID(school.getId());
			System.out
					.println("-> " + school.getName() + " - " + studentManager.calculateAverageGrade(foundedStudents));
		}
	}

	private void printIncomeBySchool(List<School> schools) {
		for (School school : schools) {
			System.out.println("-> " + school.getName() + " - "
					+ schoolManager.calculateIncome(school, studentManager.getAllStudents()));
		}
	}
}
