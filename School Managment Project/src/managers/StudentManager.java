package managers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import enums.Gender;
import generators.*;
import interfaces.NumberFormater;
import models.*;

public class StudentManager implements NumberFormater {
	private List<Student> allStudents;

	public StudentManager() {
		allStudents = new ArrayList<>();
	}

	public List<Student> getAllStudents() {
		return allStudents;
	}

	@Override
	public double formatDouble(double doubleNumber) {
		return Math.round((doubleNumber) * 100) / 100.0;
	}

	public List<Student> createStudents(int numberOfStudents, School school) {
		List<Student> newStudents = new ArrayList<>();
		Student student;

		for (int i = 0; i < numberOfStudents; i++) {
			if (i % 2 == 0) {
				student = new Student(NamesGenerator.getStudentsByGender(Gender.FEMALE).get(i), Gender.FEMALE,
						school.getId());
			} else {
				student = new Student(NamesGenerator.getStudentsByGender(Gender.MALE).get(i), Gender.MALE,
						school.getId());
			}

			updateSchool(student, school);
			newStudents.add(student);
		}

		allStudents.addAll(newStudents);

		return newStudents;
	}

	public double calculateAverageGrade(List<Student> students) {
		double sumOfGrades = 0;

		for (Student student : students) {
			sumOfGrades += student.getGrade();
		}

		return formatDouble(sumOfGrades / students.size());
	}

	public Student findTopPerformingStudent() {
		sortAllStudents();
		return allStudents.get(0);
	}

	public Student findTopPerformingStudentByGender(Gender gender) {
		sortAllStudents();

		for (Student student : allStudents) {
			if (student.getGender().equals(gender)) {
				return student;
			}
		}

		return new NullStudent();
	}

	public Student findTopContributer() {
		Student topContributer = allStudents.get(0);

		for (Student student : allStudents) {
			if (topContributer.getPayment() < student.getPayment()) {
				topContributer = student;
			}
		}

		return topContributer;
	}

	public List<Student> findAllStudentsBySchoolID(String schoolID) {
		List<Student> foundedStudents = new ArrayList<>();

		for (Student studentToSearch : allStudents) {
			if (studentToSearch.getSchoolId().equals(schoolID)) {
				foundedStudents.add(studentToSearch);
			}
		}

		return foundedStudents;
	}

	private void sortAllStudents() {
		Collections.sort(allStudents);
	}

	private void updateSchool(Student student, School school) {
		school.addNewStudent();
		school.recalculateAge(student.getAge());
	}

}
