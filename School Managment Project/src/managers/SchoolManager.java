package managers;

import java.util.ArrayList;
import java.util.List;

import generators.*;
import interfaces.NumberFormater;
import models.*;

public class SchoolManager implements NumberFormater {
	private List<School> highSchools;
	private List<School> universities;

	public SchoolManager() {
		highSchools = new ArrayList<>();
		universities = new ArrayList<>();
	}

	public List<School> getHighSchools() {
		return highSchools;
	}

	public List<School> getUniversities() {
		return universities;
	}
	
	@Override
	public double formatDouble(double doubleNumber) {
		return Math.round((doubleNumber) * 100) / 100.0;
	}
	
	public void createHighSchools(int numberOfHighSchools) {
		for (int i = 0; i < numberOfHighSchools; i++) {
			highSchools
					.add(new HighSchool(NamesGenerator.getHighSchools().get(i), NamesGenerator.getAddresses().get(i)));
		}
	}

	public void createUniversities(int numberOfUniversities) {
		for (int i = 0; i < numberOfUniversities; i++) {
			universities
					.add(new University(NamesGenerator.getUniversities().get(i), NamesGenerator.getAddresses().get(i)));
		}
	}

	public void sendMessage(List<Student> toStudents, School fromSchool) {
		for (Student student : toStudents) {
			System.out.println(student.toString() + " " + fromSchool.toString());
		}
	}

	public double calculateIncome(School school, List<Student> allStudents) {
		double income = 0;
		int schoolTax = getSchoolTax(school);

		for (Student student : allStudents) {
			income += student.calculatePayment(school.getAverageAge(), schoolTax);
		}

		return formatDouble(income);
	}

	private int getSchoolTax(School school) {
		for (School highSchool : highSchools) {
			if (highSchool.equals(school)) {
				return new HighSchool().getTax();
			}
		}

		for (School university : universities) {
			if (university.equals(school)) {
				return new University().getTax();
			}
		}

		return -1;
	}

}
