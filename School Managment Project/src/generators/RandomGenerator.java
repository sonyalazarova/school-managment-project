package generators;

import java.util.Random;
import java.util.UUID;

public class RandomGenerator {	
	private static Random random = new Random();
	
	public static double generateGrade() {
		return generateDouble(3, 6);
	}

	public static String generateId() {
		return UUID.randomUUID().toString();
	}

	public static int generateAge() {		
		return random.nextInt(10) + 15;
	}	

	public static int generateInt(int fromInt, int toInt) {
		return random.nextInt((toInt - fromInt) + 1) + fromInt;
	}
	
	private static double generateDouble(double fromDouble, double toDouble) {
		return fromDouble + (toDouble - fromDouble) * random.nextDouble();
	}
}