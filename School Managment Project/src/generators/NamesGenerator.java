package generators;

import java.util.ArrayList;
import java.util.List;

import enums.Gender;
import models.Address;

public class NamesGenerator {
	public static List<String> getHighSchools() {
		ArrayList<String> names = new ArrayList<>();

		names.add("High School of Mathematics");
		names.add("First Language School");
		names.add("High School of Economics");

		return names;
	}

	public static List<String> getUniversities() {
		ArrayList<String> names = new ArrayList<>();

		names.add("Tehnical university of Varna");
		names.add("Varna free university");

		return names;
	}

	public static List<Address> getAddresses() {
		ArrayList<Address> names = new ArrayList<>();

		names.add(new Address("Bulgaria", "Varna", "Pirin", RandomGenerator.generateInt(1, 30)));
		names.add(new Address("Bulgaria", "Sofia", "Al. Dyakovich", RandomGenerator.generateInt(1, 50)));
		names.add(new Address("Bulgaria", "Burgas", "N. Derjavin", RandomGenerator.generateInt(1, 60)));
		names.add(new Address("Bulgaria", "Plovdiv", "Struga", RandomGenerator.generateInt(1, 10)));
		names.add(new Address("Bulgaria", "V. Tyrnovo", "Slivnitsa", RandomGenerator.generateInt(1, 130)));

		return names;
	}

	public static List<String> getStudentsByGender(Gender gender) {
		List<String> names;

		if (gender.equals(Gender.FEMALE)) {
			names = getFemales();
		} else {
			names = getMales();
		}

		return names;
	}

	private static ArrayList<String> getMales() {
		ArrayList<String> names = new ArrayList<>();

		names.add("Ivan Ivanov");
		names.add("Petyr Petrov");
		names.add("Stoyan Stoyanov");
		names.add("Dimityr Dimitrov");
		names.add("Ivaylo Ivanov");
		names.add("Hristo Hristov");
		names.add("Simeon Simenov");
		names.add("Lazar Lazarov");
		names.add("Mihail Mihailov");
		names.add("Stefan Stefanov");

		return names;
	}

	private static ArrayList<String> getFemales() {
		ArrayList<String> names = new ArrayList<>();

		names.add("Krasimira Ivanova");
		names.add("Hikol Stancheva");
		names.add("Kalina Bogoeva");
		names.add("Dea Stojkova");
		names.add("Bojana Tashkova");
		names.add("Aleksandra Dimitrova");
		names.add("Viktoria Ivanova");
		names.add("Tsveteya Toshkova");
		names.add("Elena Stoyanova");
		names.add("Monika Panayotova");

		return names;
	}
}
