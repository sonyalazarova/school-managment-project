package models;

import generators.RandomGenerator;

public class HighSchool extends School {
	private static int tax = 50;

	public HighSchool(String name, Address address) {
		super(RandomGenerator.generateId(), name, address);
	}
	
	public HighSchool() {
		super();
	}
	
	public int getTax() {
		return tax;
	}
}
