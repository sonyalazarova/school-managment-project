package models;

public class School {
	private String id;
	private String name;
	private Address address;
	private double averageAge;
	private int studentsCount;

	public School(String id, String name, Address address) {
		this.id = id;
		this.name = name;
		this.address = address;
	}

	public School() {
	}

	public String getId() {
		return this.id;
	}

	public String getName() {
		return name;
	}

	public double getAverageAge() {
		return averageAge;
	}
	
	@Override
	public String toString() {
		return "Welcome to " + name;
	}

	public void addNewStudent() {
		studentsCount++;
	}

	public void recalculateAge(int newAge) {
		averageAge = (averageAge * (studentsCount - 1) + newAge) / studentsCount;
	}
}
