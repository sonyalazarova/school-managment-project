package models;

import enums.Gender;
import generators.RandomGenerator;
import interfaces.NumberFormater;

public class Student implements Comparable<Student>, NumberFormater {
	private String name;
	private int age;
	private Gender gender;
	private double grade;
	private double payment;
	private String schoolId;

	public Student() {
	}

	public Student(String name, Gender gender, String schoolId) {
		this.name = name;
		this.age = RandomGenerator.generateAge();
		this.gender = gender;
		this.grade = RandomGenerator.generateGrade();
		this.schoolId = schoolId;
	}

	@Override
	public String toString() {
		return "Hello " + name + "!";
	}

	@Override
	public int compareTo(Student student) {
		return student.grade <= grade ? -1 : 0;
	}

	@Override
	public double formatDouble(double doubleNumber) {
		return Math.round((doubleNumber) * 100) / 100.0;
	}

	public String getName() {
		return name;
	}

	public String getSchoolId() {
		return schoolId;
	}

	public double getGrade() {
		return grade;
	}

	public Gender getGender() {
		return gender;
	}

	public int getAge() {
		return age;
	}

	public double getPayment() {
		return payment;
	}

	public double calculatePayment(double averageSchoolAge, int schoolTax) {
		payment = averageSchoolAge / age * 100 + schoolTax;
		return payment;
	}
}
