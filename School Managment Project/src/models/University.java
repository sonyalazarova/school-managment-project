package models;

import generators.RandomGenerator;

public class University extends School {
	private final int tax = 100;

	public University(String name, Address address) {
		super(RandomGenerator.generateId(), name, address);
	}

	public University() {
		super();
	}

	public int getTax() {
		return tax;
	}
}
