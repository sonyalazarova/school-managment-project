
public class Main {

	public static void main(String[] args) {
		SchoolManagmentProgram schoolManagmentProgram = new SchoolManagmentProgram();
		schoolManagmentProgram.printAverageGrade();
		schoolManagmentProgram.printTopPerformingStudent();
		schoolManagmentProgram.printTopPerformersByGender();
		schoolManagmentProgram.printTotalSchoolsIncome();
		schoolManagmentProgram.printTopContributer();
	}
}
